def log(results):
    f = open("results.csv", "a")
    for i in range(len(results)):
        generation_number, best_fitness, avg_fitness, worst_fitness, filename = results[i]
        f.write(filename + ';' +
                str(generation_number) + ';' +
                str(best_fitness).replace('.', ',') + ';' +
                str(avg_fitness).replace('.', ',') + ';' +
                str(worst_fitness).replace('.', ',') +
                "\n")
    f.write('\n')
    f.close()
