class City:

    def __init__(self, city_number, x, y):
        self.city_number = city_number
        self.x = x
        self.y = y

    def __str__(self):
        return "city number: " + self.city_number + " x value: " + self.x + "y value: " + self.y


class Individual:

    def __init__(self, genotype, fitness, distance):
        self.genotype = genotype
        self.fitness = fitness
        self.distance = distance
