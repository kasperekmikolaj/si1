from models import City


def read_file(filename):
    file = open("TSP/" + filename)
    lines = [line for line in file.readlines() if line != "\n" and line != "EOF\n" and line != "EOF"]
    first_data_index = lines.index("NODE_COORD_SECTION\n") + 1
    cities = {}
    for i in range(first_data_index, len(lines)):
        lines[i].lstrip(" ")
        lines[i] = " ".join(lines[i].split())
        city_number, x, y = lines[i].split()
        city_number = int(city_number)
        x = float(x)
        y = float(y)
        cities[city_number] = City(city_number, x, y)
    file.close()
    return cities
