

def zachlanny(utils, starting_city_number):
    genotype = []
    unvisited_cities_list = list(utils.cities_dictionary.values())
    starting_city_index = starting_city_number - 1
    current_city = unvisited_cities_list[starting_city_index]
    genotype.append(current_city.city_number)
    unvisited_cities_list.pop(starting_city_index)
    while unvisited_cities_list:
        closest_city = find_closest(current_city, unvisited_cities_list, utils)
        genotype.append(closest_city.city_number)
        unvisited_cities_list.remove(closest_city)
        current_city = closest_city
    return genotype


def find_closest(city, left_cities, utils):
    smallest_distance = float('inf')
    closest_city = left_cities[0]
    for unvisited_city in left_cities:
        distance = utils.calculate_distance(city.city_number, unvisited_city.city_number)
        if distance < smallest_distance:
            smallest_distance = distance
            closest_city = unvisited_city
    return closest_city


def random_alg(utils):
    number_of_individuals = utils.population_size*utils.number_of_generations
    # number_of_individuals = 100
    fitness_list = []
    fitness_dictionary = {}
    for _ in range(number_of_individuals):
        genotype = utils.generate_random_genotype()
        fitness = utils.fitness_function(genotype)
        fitness_list.append(fitness)
        fitness_dictionary[fitness] = genotype
    fitness_list.sort(reverse=True)
    best_fitness = fitness_list[0]
    return fitness_dictionary[best_fitness]





