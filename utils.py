import math
import random

from data_reader import read_file


class Utils:
    number_of_best_to_save = 5
    population_size = 100
    number_of_generations = 5000
    mutation_prob = 0.3
    crossover_prob = 0.65
    tournament_size = 5

    def __init__(self, filename):
        self.filename = filename
        self.cities_dictionary = read_file(filename)

    def generate_random_genotype(self):
        list_copy = list(self.cities_dictionary.values()).copy()
        random.shuffle(list_copy)
        return [city.city_number for city in list_copy]

    def distance_function(self, genotype):
        truck_distance = 0
        current_index = 0
        for next_index in range(1, len(genotype)):
            truck_distance += self.calculate_distance(genotype[current_index], genotype[next_index])
            current_index += 1
        return truck_distance

    def calculate_distance(self, first_city_number, second_city_number):
        first_city = self.cities_dictionary.get(first_city_number)
        second_city = self.cities_dictionary.get(second_city_number)
        distance = math.sqrt((first_city.x - second_city.x) ** 2 + (first_city.y - second_city.y) ** 2)
        return distance

    def fitness_function(self, genotype):
        return 100000 / self.distance_function(genotype)

    def swap_mutation(self, genotype):
        # genotype_length = len(genotype)
        # for first_gen_to_swap in range(genotype_length):
        #     mutation_probability = random.random()
        #     if mutation_probability < Utils.mutation_prob:
        #         second_gen_to_swap = random.randint(0, genotype_length - 1)
        #         while second_gen_to_swap == first_gen_to_swap:
        #             second_gen_to_swap = random.randint(0, genotype_length - 1)
        #         temp = genotype[first_gen_to_swap]
        #         genotype[first_gen_to_swap] = genotype[second_gen_to_swap]
        #         genotype[second_gen_to_swap] = temp
        genotype_length = len(genotype)
        first_gen_to_swap = random.randint(0, genotype_length - 1)
        mutation_probability = random.random()
        if mutation_probability < Utils.mutation_prob:
            second_gen_to_swap = random.randint(0, genotype_length - 1)
            while second_gen_to_swap == first_gen_to_swap:
                second_gen_to_swap = random.randint(0, genotype_length - 1)
            temp = genotype[first_gen_to_swap]
            genotype[first_gen_to_swap] = genotype[second_gen_to_swap]
            genotype[second_gen_to_swap] = temp


    def ordered_crossover(self, first_genotype, second_genotype):
        genotype_length = len(first_genotype)
        result = [-1 for _ in range(genotype_length)]
        first_index = random.randint(0, genotype_length - 1)
        second_index = random.randint(0, genotype_length - 1)
        while second_index == first_index:
            second_index = random.randint(0, genotype_length - 1)
        if second_index < first_index:
            temp = first_index
            first_index = second_index
            second_index = temp
        for i in range(first_index, second_index):
            result[i] = first_genotype[i]
        unvisited_cities = []
        for i in range(genotype_length):
            if second_genotype[i] not in result:
                unvisited_cities.append(second_genotype[i])
        for i in range(first_index):
            result[i] = unvisited_cities[i]
        next_index = first_index
        for i in range(second_index, genotype_length):
            result[i] = unvisited_cities[next_index]
            next_index += 1
        return result

    def tournament_selection(self, population):
        competitors_indexes = []
        while len(competitors_indexes) != Utils.tournament_size:
            index = random.randint(0, len(population) - 1)
            while index in competitors_indexes:
                index = random.randint(0, len(population) - 1)
            competitors_indexes.append(index)
        competitors_dictionary = {}
        for i in competitors_indexes:
            individual = population[i]
            competitors_dictionary[individual.fitness] = individual
        key_list = list(competitors_dictionary.keys())
        key_list.sort(reverse=True)
        best_fitness = key_list[0]
        return competitors_dictionary[best_fitness]

    def roulette_selection(self, population):
        fitness_sum = sum(self.fitness_function(individual.genotype) for individual in population)
        starting_prob = 0
        finish_prob = self.fitness_function(population[0].genotype) / fitness_sum
        probability_list = [(starting_prob, finish_prob, population[0])]
        for i in range(1, len(population)):
            starting_prob = finish_prob
            current_individual = population[i]
            finish_prob += self.fitness_function(current_individual.genotype) / fitness_sum
            probability_list.append((starting_prob, finish_prob, current_individual))
        cursor = random.random()
        for i in range(len(probability_list)):
            start_p, finish_p, indiv = probability_list[i]
            if start_p <= cursor <= finish_p:
                return indiv
