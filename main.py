import random

import logger
from models import Individual
from utils import Utils
from zachlanny_i_random import zachlanny, random_alg

if __name__ == "__main__":
    # utils = Utils("berlin11_modified.tsp")
    utils = Utils("kroA100.tsp")

    genotype = zachlanny(utils, 2)
    print(genotype)
    print(len(genotype))
    print(str(utils.distance_function(genotype)) + ' <- zachlanny dystans')
    print(str(utils.fitness_function(genotype)) + ' <- zachlanny fitness')

    # gen_random = random_alg(utils)
    # print(str(utils.distance_function(gen_random)) + ' <- random')
    # print(str(utils.fitness_function(gen_random)) + ' <- random')
    #

    population = [utils.generate_random_genotype() for _ in range(Utils.population_size)]
    population = [Individual(genotype,
                             utils.fitness_function(genotype),
                             utils.distance_function(genotype)) for genotype in population]
    results = []
    best_distance = float('inf')
    for i in range(Utils.number_of_generations):
        new_population = []
        population.sort(key=lambda individual: individual.fitness, reverse=True)
        for j in range(Utils.number_of_best_to_save):
            new_population.append(population[j])
        while len(new_population) != Utils.population_size:

            first_selected_individual = utils.tournament_selection(population)
            second_selected_individual = utils.tournament_selection(population)
            # first_selected_individual = utils.roulette_selection(population)
            # second_selected_individual = utils.roulette_selection(population)

            while first_selected_individual == second_selected_individual:

                second_selected_individual = utils.tournament_selection(population)
                # second_selected_individual = utils.roulette_selection(population)

            crossover_probability = random.random()
            child_genotype = None
            if crossover_probability < Utils.crossover_prob:
                child_genotype = utils.ordered_crossover(first_selected_individual.genotype,
                                                         second_selected_individual.genotype)
            else:
                if first_selected_individual in new_population:
                    child_genotype = second_selected_individual.genotype
                else:
                    child_genotype = second_selected_individual.genotype
            utils.swap_mutation(child_genotype)
            child = Individual(child_genotype,
                               utils.fitness_function(child_genotype),
                               utils.distance_function(child_genotype))
            new_population.append(child)
        population = new_population

        # zapis danych
        avg_fitness = sum(i.fitness for i in population)/len(population)
        best_fitness = max(i.fitness for i in population)
        current_best_distance = max(i.distance for i in population)
        if current_best_distance < best_distance:
            best_distance = current_best_distance
        worst_fitness = min(i.fitness for i in population)
        results.append((i+1, best_fitness, avg_fitness, worst_fitness, utils.filename))
    logger.log(results)
    print(str(best_distance) + " <- genetyczny - dystans")







